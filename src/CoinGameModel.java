/**
 * An interface for playing a coin game. The rules of a particular coin game
 * will be implemented by classes that implement this interface.
 */
public interface CoinGameModel {
  /**
   * Gets the size of the board (the number of squares)
   *
   * @return the board size
   */
  int boardSize();

  /**
   * Gets the number of coins.
   *
   * @return the number of coins
   */
  int coinCount();

  /**
   * Gets the (zero-based) position of coin number {@code coinIndex}.
   *
   * @param coinIndex which coin to look up
   * @return the coin's position
   * @throws IllegalArgumentException
   *     if there is no coin with the requested index
   */
  int getCoinPosition(int coinIndex);

  /**
   * Returns whether the current game is over. The game is over if there are
   * no valid moves.
   *
   * @return whether the game is over
   */
  boolean isGameOver();

  /**
   * Moves coin number {@code coinIndex} to position {@code newPosition}.
   * Throws {@code IllegalMoveException} if the requested move is illegal,
   * which can happen in several ways:
   *
   * <ul>
   *   <li>There is no coin with the requested index.</li>\
   *   <li>The new position is occupied by another coin.</li>
   *   <li>There is some other reason the move is illegal,
   *       as specified by the concrete game class.</li>
   * </ul>
   *
   * Note that {@code coinIndex} refers to the coins as numbered from 0
   * to {@code coinCount() - 1}, not their absolute position on the board.
   * However, coins have no identity, so if one coin passes another, their
   * indices are exchanged. The leftmost coin is always coin 0, the next
   * leftmost is coin 1, and so on.
   *
   * @param coinIndex   which coin to move (numbered from the left)
   * @param newPosition where to move it to
   * @throws IllegalMoveException the move is illegal
   */
  void move(int coinIndex, int newPosition);

  /**
   * Adds a new player to the end of the turn cycle.
   *
   * @throws IllegalStateException if trying to add a new player when the game is already over
   */
  void addPlayer();

  /**
   * Adds a new player to the game at the given position.
   *
   * @param pos the position in the turn cycle to insert the player
   * @throws IllegalStateException     if trying to add a new player when the game is already over
   * @throws IndexOutOfBoundsException if {@code pos} is < 0 or {@code pos} > size()
   */
  void addPlayer(int pos);

  /**
   * Counts the number of players in the game.
   *
   * @return number of players in the game
   */
  int getNumberOfPlayers();

  /**
   * Determines which player's turn it currently is.
   *
   * @return player whose currentPlayerTurn it is
   * @throws IllegalStateException if invoked when game is over
   */
  int whoseTurn();

  /**
   * Determines the player who won the game.
   *
   * Note: the winner of the game is the player who made the last move before the game is over
   *
   * @return the winner of the game
   * @throws IllegalStateException if invoked when game is not over
   */
  int getWinner();

  /**
   * The exception thrown by {@code move} when the requested move is illegal.
   */
  class IllegalMoveException extends IllegalArgumentException {
    /**
     * Constructs a illegal move exception with no description.
     */
    public IllegalMoveException() {
      super();
    }

    /**
     * Constructs a illegal move exception with the given description.
     *
     * @param msg the description
     */
    public IllegalMoveException(String msg) {
      super(msg);
    }
  }
}
