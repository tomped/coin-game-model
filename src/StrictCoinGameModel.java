import java.util.ArrayList;

/**
 * To represent a Strict Coin Game
 */
public final class StrictCoinGameModel implements CoinGameModel {
  private final boolean[] board;
  private final ArrayList<Integer> players = new ArrayList<>();
  private int coinCount = 0;
  private int currentPlayerTurn = 0;

  /**
   * INVARIANT: players.length() must be > than zero
   * INVARIANT: coinCount must be >= 0
   * INVARIANT: currentPlayerTurn must be a integer >= 0 and < getNumberOfPlayers()
   */

  /**
   * Constructs a Strict Coin Game Model with one player and a given board
   *
   * @param board the board of the game
   * @throws IllegalArgumentException if board is null
   * @throws IllegalArgumentException if board contains another characters than 'O' and '-'
   */
  public StrictCoinGameModel(String board) throws IllegalArgumentException {
    if (board == null) {
      throw new IllegalArgumentException("Not valid input to start a game.");
    }
    this.players.add(0);
    this.board = new boolean[board.length()];
    this.buildBoard(board);
  }

  /**
   * Constructs a Strict Coin Game Model with a given number of players and a given board
   *
   * @param numberOfPlayers number of players
   * @param board           the board of the game
   * @throws IllegalArgumentException if the number of players is a not a natural number
   * @throws IllegalArgumentException if board is null
   * @throws IllegalArgumentException if board contains other characters than 'O' and '-'
   */
  public StrictCoinGameModel(int numberOfPlayers, String board) throws IllegalArgumentException {
    if (board == null || numberOfPlayers <= 0) {
      throw new IllegalArgumentException("Not valid input to start a game.");
    }
    for (int i = 0; i < numberOfPlayers; i++) {
      this.players.add(i);
    }
    this.board = new boolean[board.length()];
    this.buildBoard(board);
  }

  /**
   * Builds the board of coins represented by booleans
   *
   * @param board the board of the game
   * @throws IllegalArgumentException if the board is not valid
   */
  private void buildBoard(String board) throws IllegalArgumentException {
    for (int i = 0; i < board.length(); i++) {
      if (board.charAt(i) == 'O') {
        this.coinCount++;
        this.board[i] = true;
      } else if (board.charAt(i) == '-') {
        this.board[i] = false;
      } else {
        throw new IllegalArgumentException("Not a valid board.");
      }
    }
  }

  /**
   * Gets the size of the board (the number of squares)
   *
   * @return the board size
   */
  @Override
  public int boardSize() {
    return this.board.length;
  }

  /**
   * Gets the number of coins.
   *
   * @return the number of coins
   */
  @Override
  public int coinCount() {
    return this.coinCount;
  }

  /**
   * Gets the (zero-based) position of coin number {@code coinIndex}.
   *
   * @param coinIndex which coin to look up
   * @return the coin's position
   * @throws IllegalArgumentException if there is no coin with the requested index
   */
  @Override
  public int getCoinPosition(int coinIndex) throws IllegalArgumentException {
    int coinsPassed = 0;
    int currentCoinPosition;
    if (coinIndex < 0 || coinIndex >= coinCount()) {
      throw new IllegalArgumentException("No coin with the requested index.");
    }
    for (currentCoinPosition = 0; coinsPassed <= coinIndex; currentCoinPosition++) {
      if (this.board[currentCoinPosition]) {
        coinsPassed++;
      }
    }
    return currentCoinPosition - 1;
  }

  /**
   * Returns whether the current game is over. The game is over if there are no valid moves.
   *
   * @return whether the game is over
   */
  @Override
  public boolean isGameOver() {
    for (int i = 0; i < coinCount(); i++) {
      if (!this.board[i]) {
        return false;
      }
    }
    return true;
  }

  /**
   * Moves coin number {@code coinIndex} to position {@code newPosition}.
   * Throws {@code IllegalMoveException} if the requested move is illegal,
   * which can happen in several ways:
   *
   * <ul>
   *   <li>There is no coin with the requested index.</li>
   *   <li>The new position is occupied by another coin.</li>
   *   <li>You cannot move a coin past another coin</li>
   *   <li>You cannot move a coin to the right</li>
   * </ul>
   *
   * Note that {@code coinIndex} refers to the coins as numbered from 0
   * to {@code coinCount() - 1}, not their absolute position on the board.
   *
   * @param coinIndex   which coin to move (numbered from the left)
   * @param newPosition where to move it to
   * @throws IllegalMoveException the move is illegal
   */
  @Override
  public void move(int coinIndex, int newPosition) throws IllegalMoveException {
    if (isValidMove(coinIndex, newPosition)) {
      this.board[getCoinPosition(coinIndex)] = false;
      this.board[newPosition] = true;
      if (!isGameOver()) {
        this.currentPlayerTurn = (this.currentPlayerTurn + 1) % getNumberOfPlayers();
      }
    }
  }

  /**
   * Moves coin number {@code coinIndex} to position {@code newPosition}.
   * Throws {@code IllegalMoveException} if the requested move is illegal,
   * which can happen in several ways:
   *
   * <ul>
   *   <li>There is no coin with the requested index.</li>
   *   <li>The new position is occupied by another coin.</li>
   *   <li>You cannot move a coin past another coin</li>
   *   <li>You cannot move a coin to the right</li>
   * </ul>
   *
   * Note that {@code coinIndex} refers to the coins as numbered from 0
   * to {@code coinCount() - 1}, not their absolute position on the board.
   *
   * @param coinIndex   which coin to move (numbered from the left)
   * @param newPosition where to move it to
   * @throws IllegalMoveException the move is illegal
   */
  public boolean isValidMove(int coinIndex, int newPosition) throws IllegalMoveException {
    if (coinIndex >= coinCount() || coinIndex < 0) {
      throw new IllegalMoveException("No coin with the requested index.");
    }
    if (newPosition < 0 || newPosition > boardSize()) {
      throw new IllegalMoveException("Position doesn't exist.");
    }
    if (newPosition > getCoinPosition(coinIndex)) {
      throw new IllegalMoveException("Can't move coins to the right.");
    }
    if (this.board[newPosition]) {
      throw new IllegalMoveException("Already a coin at that position.");
    }
    for (int i = newPosition; i < getCoinPosition(coinIndex); i++) {
      if (this.board[i]) {
        throw new IllegalMoveException("You can't move a coin past another coin.");
      }
    }
    return true;
  }

  /**
   * Adds a new player to the end of the turn cycle.
   *
   * @throws IllegalStateException if trying to add a new player when the game is already over
   */
  @Override
  public void addPlayer() throws IllegalStateException {
    if (this.isGameOver()) {
      throw new IllegalStateException("Can't add a player when the game is already over.");
    }
    this.players.add(getNumberOfPlayers());
  }

  /**
   * Adds a new player to the game at the given position.
   *
   * @param pos the position in the turn cycle to insert the player
   * @throws IllegalStateException     if trying to add a new player when the game is already over
   * @throws IndexOutOfBoundsException if {@code pos} is < 0 or {@code pos} > size()
   */
  @Override
  public void addPlayer(int pos) throws IllegalStateException {
    if (this.isGameOver()) {
      throw new IllegalStateException("Can't add a player when the game is already over.");
    }
    if (pos <= this.currentPlayerTurn) {
      this.currentPlayerTurn++;
    }
    this.players.add(pos, getNumberOfPlayers());
  }

  /**
   * Counts the number of players in the game.
   *
   * @return number of players in the game
   */
  @Override
  public int getNumberOfPlayers() {
    return this.players.size();
  }

  /**
   * Determines which player's turn it currently is.
   *
   * @return player whose currentPlayerTurn it is
   * @throws IllegalStateException if invoked when game is over
   */
  @Override
  public int whoseTurn() throws IllegalStateException {
    if (this.isGameOver()) {
      throw new IllegalStateException("The game is already over.");
    }
    return this.players.get(this.currentPlayerTurn);
  }

  /**
   * Determines the player who won the game.
   *
   * Note: the winner of the game is the player who made the last move before the game is over
   *
   * @return the winner of the game
   * @throws IllegalStateException if invoked when game is not over
   */
  @Override
  public int getWinner() throws IllegalStateException {
    if (!this.isGameOver()) {
      throw new IllegalStateException("The game isn't over, so there is no winner yet.");
    }
    return this.currentPlayerTurn;
  }

  /**
   * Produces the state of the board in the form of a string with 'O's and '-'s
   *
   * @return string of the game board
   */
  public String toString() {
    String board = "";
    for (int i = 0; i < this.boardSize(); i++) {
      if (this.board[i]) {
        board += "O";
      } else {
        board += "-";
      }
    }
    return board;
  }
}
