import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Tests for StrictCoinGameModel
 */
public class CoinGameModelTest {
  @Test
  public void testValidConstructor1() {
    assertEquals("O---", new StrictCoinGameModel("O---").toString());
  }
  @Test
  public void testValidConstructor2() {
    assertEquals("----", new StrictCoinGameModel("----").toString());
  }
  @Test
  public void testValidConstructor3() {
    assertEquals("-O--", new StrictCoinGameModel("-O--").toString());
  }
  @Test
  public void testValidConstructor4() {
    assertEquals("-O--O--", new StrictCoinGameModel("-O--O--").toString());
  }
  @Test
  public void testValidConstructor5() {
    assertEquals("OOOOO", new StrictCoinGameModel("OOOOO").toString());
  }
  @Test
  public void testValidConstructor6() {
    assertEquals("------", new StrictCoinGameModel("------").toString());
  }
  @Test(expected = IllegalArgumentException.class)
  public void testInvalidConstructor1() {
    new StrictCoinGameModel("-x--");
  }
  @Test(expected = IllegalArgumentException.class)
  public void testInvalidConstructor2() {
    new StrictCoinGameModel(null);
  }
  @Test(expected = IllegalArgumentException.class)
  public void testInvalidConstructor3() {
    new StrictCoinGameModel("x");
  }
  @Test
  public void testValidConstructor7() {
    assertEquals("O---", new StrictCoinGameModel(1, "O---").toString());
  }
  @Test
  public void testValidConstructor8() {
    assertEquals("----", new StrictCoinGameModel(2, "----").toString());
  }
  @Test
  public void testValidConstructor9() {
    assertEquals("-O--", new StrictCoinGameModel(2, "-O--").toString());
  }
  @Test
  public void testValidConstructor10() {
    assertEquals("-O--O--", new StrictCoinGameModel(2, "-O--O--").toString());
  }
  @Test
  public void testValidConstructor11() {
    assertEquals("OOOOO", new StrictCoinGameModel(2, "OOOOO").toString());
  }
  @Test
  public void testValidConstructor12() {
    assertEquals("------", new StrictCoinGameModel(1, "------").toString());
  }
  @Test(expected = IllegalArgumentException.class)
  public void testInvalidConstructor4() {
    new StrictCoinGameModel(1, "-x--");
  }
  @Test(expected = IllegalArgumentException.class)
  public void testInvalidConstructor5() {
    new StrictCoinGameModel(2, null);
  }
  @Test(expected = IllegalArgumentException.class)
  public void testInvalidConstructor6() {
    new StrictCoinGameModel(2, "x");
  }
  @Test(expected = IllegalArgumentException.class)
  public void testInvalidConstructor7() {
    new StrictCoinGameModel(-2, "---O--");
  }
  @Test
  public void testBoardSize1() {
    assertEquals(4, new StrictCoinGameModel("O---").boardSize());
  }
  @Test
  public void testBoardSize2() {
    assertEquals(3, new StrictCoinGameModel("---").boardSize());
  }
  @Test
  public void testBoardSize3() {
    assertEquals(1, new StrictCoinGameModel("O").boardSize());
  }
  @Test
  public void testCoinCount1() {
    assertEquals(1, new StrictCoinGameModel("O---").coinCount());
  }
  @Test
  public void testCoinCount2() {
    assertEquals(0, new StrictCoinGameModel("---").coinCount());
  }
  @Test
  public void testCoinCount3() {
    assertEquals(3, new StrictCoinGameModel("OOO").coinCount());
  }
  @Test
  public void testCoinCount4() {
    assertEquals(0, new StrictCoinGameModel("").coinCount());
  }
  @Test
  public void testGetCoinPosition1() {
    assertEquals(0, new StrictCoinGameModel("O---").getCoinPosition(0));
  }
  @Test
  public void testGetCoinPosition2() {
    assertEquals(2, new StrictCoinGameModel("OOO").getCoinPosition(2));
  }
  @Test(expected = IllegalArgumentException.class)
  public void testInvalidGetCoinPosition1() {
    new StrictCoinGameModel("---").getCoinPosition(1);
  }
  @Test(expected = IllegalArgumentException.class)
  public void testInvalidGetCoinPosition2() {
    new StrictCoinGameModel("O--").getCoinPosition(1);
  }
  @Test(expected = IllegalArgumentException.class)
  public void testInvalidGetCoinPosition3() {
    new StrictCoinGameModel("O--").getCoinPosition(-1);
  }
  @Test
  public void testIsGameOver1() {
    assertTrue(new StrictCoinGameModel("O---").isGameOver());
  }
  @Test
  public void testIsGameOver2() {
    assertTrue(new StrictCoinGameModel("OOOO").isGameOver());
  }
  @Test
  public void testIsGameOver3() {
    assertFalse(new StrictCoinGameModel("O-O-").isGameOver());
  }
  @Test
  public void testIsGameOver4() {
    assertFalse(new StrictCoinGameModel("-OO-").isGameOver());
  }
  @Test
  public void testIsGameOver5() {
    assertTrue(new StrictCoinGameModel("----").isGameOver());
  }
  @Test
  public void testIsGameOver6() {
    assertTrue(new StrictCoinGameModel("").isGameOver());
  }
  @Test
  public void testToString1() {
    assertEquals("----", new StrictCoinGameModel("----").toString());
  }
  @Test
  public void testToString2() {
    assertEquals("-", new StrictCoinGameModel("-").toString());
  }
  @Test
  public void testToString3() {
    assertEquals("O", new StrictCoinGameModel("O").toString());
  }
  @Test
  public void testToString4() {
    assertEquals("O--O--O", new StrictCoinGameModel("O--O--O").toString());
  }
  @Test
  public void testMove1() {
    StrictCoinGameModel game = new StrictCoinGameModel("---O");
    game.move(0, 0);
    assertEquals("O---", game.toString());
  }
  @Test
  public void testMove2() {
    StrictCoinGameModel game = new StrictCoinGameModel("-O-O");
    game.move(0, 0);
    game.move(1, 1);
    assertEquals("OO--", game.toString());
  }
  @Test
  public void testMove3() {
    StrictCoinGameModel game = new StrictCoinGameModel("-O---O");
    game.move(0, 0);
    game.move(1, 3);
    assertEquals("O--O--", game.toString());
  }
  @Test(expected = CoinGameModel.IllegalMoveException.class)
  public void testInvalidMove1() {
    StrictCoinGameModel game = new StrictCoinGameModel("---");
    game.move(0, 0);
  }
  @Test(expected = CoinGameModel.IllegalMoveException.class)
  public void testInvalidMove2() {
    StrictCoinGameModel game = new StrictCoinGameModel("-O-");
    game.move(1, 0);
  }
  @Test(expected = CoinGameModel.IllegalMoveException.class)
  public void testInvalidMove3() {
    StrictCoinGameModel game = new StrictCoinGameModel("-O-");
    game.move(-1, 0);
  }
  @Test(expected = CoinGameModel.IllegalMoveException.class)
  public void testInvalidMove4() {
    StrictCoinGameModel game = new StrictCoinGameModel("O---");
    game.move(0, 0);
  }
  @Test(expected = CoinGameModel.IllegalMoveException.class)
  public void testInvalidMove5() {
    StrictCoinGameModel game = new StrictCoinGameModel("O-O-");
    game.move(1, 0);
  }
  @Test(expected = CoinGameModel.IllegalMoveException.class)
  public void testInvalidMove6() {
    StrictCoinGameModel game = new StrictCoinGameModel("-O--");
    game.move(0, 10);
  }
  @Test(expected = CoinGameModel.IllegalMoveException.class)
  public void testInvalidMove7() {
    StrictCoinGameModel game = new StrictCoinGameModel("-O--");
    game.move(0, -1);
  }
  @Test(expected = CoinGameModel.IllegalMoveException.class)
  public void testInvalidMove8() {
    StrictCoinGameModel game = new StrictCoinGameModel("-O--");
    game.move(0, 2);
  }
  @Test(expected = CoinGameModel.IllegalMoveException.class)
  public void testInvalidMove9() {
    StrictCoinGameModel game = new StrictCoinGameModel("-O--O");
    game.move(1, 0);
  }
  @Test(expected = CoinGameModel.IllegalMoveException.class)
  public void testIsValidMove1() {
    StrictCoinGameModel game = new StrictCoinGameModel("---");
    game.isValidMove(0, 0);
  }
  @Test(expected = CoinGameModel.IllegalMoveException.class)
  public void testIsValidMove2() {
    StrictCoinGameModel game = new StrictCoinGameModel("-O-");
    game.isValidMove(1, 0);
  }
  @Test(expected = CoinGameModel.IllegalMoveException.class)
  public void testIsValidMove3() {
    StrictCoinGameModel game = new StrictCoinGameModel("-O-");
    game.isValidMove(-1, 0);
  }
  @Test(expected = CoinGameModel.IllegalMoveException.class)
  public void testIsValidMove4() {
    StrictCoinGameModel game = new StrictCoinGameModel("O---");
    game.isValidMove(0, 0);
  }
  @Test(expected = CoinGameModel.IllegalMoveException.class)
  public void testIsValidMove5() {
    StrictCoinGameModel game = new StrictCoinGameModel("O-O-");
    game.isValidMove(1, 0);
  }
  @Test(expected = CoinGameModel.IllegalMoveException.class)
  public void testIsValidMove6() {
    StrictCoinGameModel game = new StrictCoinGameModel("-O--");
    game.isValidMove(0, 10);
  }
  @Test(expected = CoinGameModel.IllegalMoveException.class)
  public void testIsValidMove7() {
    StrictCoinGameModel game = new StrictCoinGameModel("-O--");
    game.isValidMove(0, -1);
  }
  @Test(expected = CoinGameModel.IllegalMoveException.class)
  public void testIsValidMove8() {
    StrictCoinGameModel game = new StrictCoinGameModel("-O--");
    game.isValidMove(0, 2);
  }
  @Test
  public void testAddPlayer1() {
    StrictCoinGameModel game = new StrictCoinGameModel("O----O");
    assertEquals(1, game.getNumberOfPlayers());
    game.addPlayer();
    assertEquals(2, game.getNumberOfPlayers());
  }
  @Test
  public void testAddPlayer2() {
    StrictCoinGameModel game = new StrictCoinGameModel(10, "---OO----");
    assertEquals(10, game.getNumberOfPlayers());
    game.addPlayer();
    game.addPlayer();
    game.addPlayer();
    assertEquals(13, game.getNumberOfPlayers());
  }
  @Test
  public void testAddPlayer3() {
    StrictCoinGameModel game = new StrictCoinGameModel("O---O--");
    game.addPlayer(0);
    assertEquals(0, game.whoseTurn());
    game.move(1, 2);
    assertEquals(1, game.whoseTurn());
  }
  @Test
  public void testAddPlayer4() {
    StrictCoinGameModel game = new StrictCoinGameModel("O---O-");
    game.addPlayer(1);
    assertEquals(0, game.whoseTurn());
    game.move(1, 2);
    assertEquals(1, game.whoseTurn());
  }
  @Test(expected = IndexOutOfBoundsException.class)
  public void testInvalidAddPlayer1() {
    StrictCoinGameModel game = new StrictCoinGameModel("O---OO--");
    game.addPlayer(-1);
  }
  @Test(expected = IndexOutOfBoundsException.class)
  public void testInvalidAddPlayer2() {
    StrictCoinGameModel game = new StrictCoinGameModel(2, "O---OO--");
    game.addPlayer(10);
  }
  @Test(expected = IllegalStateException.class)
  public void testInvalidAddPlayer3() {
    StrictCoinGameModel game = new StrictCoinGameModel("OO--O");
    game.move(2, 2);
    game.addPlayer();
  }
  @Test(expected = IllegalStateException.class)
  public void testInvalidAddPlayer4() {
    StrictCoinGameModel game = new StrictCoinGameModel("OO--O");
    game.move(2, 2);
    game.addPlayer(0);
  }
  @Test
  public void testGetNumberOfPlayers1() {
    StrictCoinGameModel game = new StrictCoinGameModel("OO");
    assertEquals(1, game.getNumberOfPlayers());
  }
  @Test
  public void testGetNumberOfPlayers2() {
    StrictCoinGameModel game = new StrictCoinGameModel(4, "OO");
    assertEquals(4, game.getNumberOfPlayers());
  }
  @Test
  public void testGetNumberOfPlayers3() {
    StrictCoinGameModel game = new StrictCoinGameModel(4, "O---O");
    game.addPlayer();
    assertEquals(5, game.getNumberOfPlayers());
  }
  @Test
  public void testWhoseTurn1() {
    StrictCoinGameModel game = new StrictCoinGameModel("O-OO-----O");
    assertEquals(0, game.whoseTurn());
    game.move(1, 1);
    assertEquals(0, game.whoseTurn());
  }
  @Test
  public void testWhoseTurn2() {
    StrictCoinGameModel game = new StrictCoinGameModel("O-OO-----O");
    assertEquals(0, game.whoseTurn());
    game.move(1, 1);
    assertEquals(0, game.whoseTurn());
    game.addPlayer();
    assertEquals(0, game.whoseTurn());
    game.move(2, 2);
    assertEquals(1, game.whoseTurn());
  }
  @Test
  public void testWhoseTurn3() {
    StrictCoinGameModel game = new StrictCoinGameModel(4, "O-------OOOOOOOOOOO");
    assertEquals(0, game.whoseTurn());
    game.move(1, 1);
    assertEquals(1, game.whoseTurn());
    game.move(2, 2);
    assertEquals(2, game.whoseTurn());
    game.move(3, 3);
    assertEquals(3, game.whoseTurn());
    game.move(4, 4);
    game.addPlayer();
    assertEquals(0, game.whoseTurn());
    game.move(5, 5);
    assertEquals(1, game.whoseTurn());
  }
  @Test
  public void testWhoseTurn4() {
    StrictCoinGameModel game = new StrictCoinGameModel("-O-O--O");
    assertEquals(0, game.whoseTurn());
    game.addPlayer(0);
    assertEquals(0, game.whoseTurn());
    game.move(0, 0);
    assertEquals(1, game.whoseTurn());
  }
  @Test
  public void testWhoseTurn5() {
    StrictCoinGameModel game = new StrictCoinGameModel(2, "--O--O");
    assertEquals(0, game.whoseTurn());
    game.addPlayer(1);
    assertEquals(0, game.whoseTurn());
    game.move(0, 0);
    assertEquals(2, game.whoseTurn());
  }
  @Test(expected = IllegalStateException.class)
  public void testInvalidWhoseTurn1() {
    StrictCoinGameModel game = new StrictCoinGameModel("---O---O");
    game.move(0, 0);
    game.move(1, 1);
    game.whoseTurn();
  }
  @Test(expected = IllegalStateException.class)
  public void testInvalidGetWinner1() {
    StrictCoinGameModel game = new StrictCoinGameModel("O-OO-----O");
    game.getWinner();
  }
  @Test(expected = IllegalStateException.class)
  public void testInvalidGetWinner2() {
    StrictCoinGameModel game = new StrictCoinGameModel("O-OO-----O");
    game.addPlayer();
    game.getWinner();
  }
  @Test(expected = IllegalStateException.class)
  public void testInvalidGetWinner3() {
    StrictCoinGameModel game = new StrictCoinGameModel(10, "O-OO-----O");
    game.getWinner();
  }
  @Test
  public void testGetWinner1() {
    StrictCoinGameModel game = new StrictCoinGameModel("-O");
    game.move(0, 0);
    assertEquals(0, game.getWinner());
  }
  @Test
  public void testGetWinner2() {
    StrictCoinGameModel game = new StrictCoinGameModel("--O");
    game.addPlayer();
    game.move(0, 1);
    game.move(0, 0);
    assertEquals(1, game.getWinner());
  }
  @Test
  public void testGetWinner3() {
    StrictCoinGameModel game = new StrictCoinGameModel(3, "---OO");
    game.move(0, 1);
    game.move(0, 0);
    game.move(1, 2);
    game.move(1, 1);
    assertEquals(0, game.getWinner());
  }
}
